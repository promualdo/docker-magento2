#!/bin/bash
service php7.0-fpm start
service mysql start
#
# Create DB schema
#
mysql --user=root --password=root -e "create database if not exists magentodb;"
mysql --user=root --password=root -e "create user if not exists magentouser@localhost identified by 'magentouser@';"
mysql --user=root --password=root -e "grant all privileges on magentodb.* to magentouser@localhost identified by 'magentouser@';"
mysql --user=root --password=root -e "flush privileges;"
#
# Install Magento
#
/usr/share/nginx/html/magento2/bin/magento setup:install --base-url=$MAGENTO_BASE_URL/ \
--db-host=localhost --db-name=magentodb --db-user=magentouser --db-password=magentouser@ \
--backend-frontname=admin --admin-firstname=Magento --admin-lastname=User --admin-email=user@example.com \
--admin-user=admin --admin-password=admin123 --language=en_GB \
--currency=EUR --timezone=Europe/London --use-rewrites=1
#
# Make files writable by NGINX
#
chown -R www-data:www-data /usr/share/nginx/html/magento2
#
# Don't let nginx end after spawning the child process workers
# If this option is not used, Docker will close because there is no processo on the foreground.
#
nginx -g 'daemon off;'
#/bin/bash