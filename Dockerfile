# Docker file for Magento2 development
# nginx:stable (1.10.2) is based on debian:jessie (8)
FROM nginx:stable
MAINTAINER romualdo
#
# Expose Port 80 - NGINX
# Expose Port 3306 - Mysql
#
EXPOSE 80 3306
#
# Magento 2.1 + PHP 7.0.13 + MySQL 5.7
#
# PHP 7.0.13
RUN apt-get -y update
# Need wget to runn install commands for PHP
RUN apt-get -y install wget
RUN apt-get -y install vim
# Add this two lines to the apt config file
# to access the PHP repository
RUN sed -i '$ a\deb http://packages.dotdeb.org jessie all' /etc/apt/sources.list
RUN sed -i '$ a\deb-src http://packages.dotdeb.org jessie all' /etc/apt/sources.list
# Download the dotdeb repository public key and add it ti the trusted keys
RUN wget -P /tmp https://www.dotdeb.org/dotdeb.gpg
RUN apt-key add /tmp/dotdeb.gpg
# Update apt cache with the contents of the new repository
RUN apt-get -y update
#
# Install PHP 7.0
#
RUN apt-get -y install php7.0
#
# Install PHP Modules required by Magento 2
# http://devdocs.magento.com/guides/v2.0/install-gde/system-requirements-2.0-tech.html
#
RUN apt-get install -y php-curl
RUN apt-get install -y php-gd
RUN apt-get install -y php7.0-imagick
RUN apt-get install -y php7.0-intl
RUN apt-get install -y php7.0-mbstring
RUN apt-get install -y php7.0-mcrypt
RUN apt-get install -y php7.0-xml
RUN apt-get install -y php7.0-soap
RUN apt-get install -y php7.0-zip
RUN apt-get install -y php7.0-json
RUN apt-get install -y php7.0-mysql
#
# Install MySQL 5.7
#
# We need an expect script to configure the mysql 5.7 package
#
RUN apt-get install -y expect
#
# Get the package definition of mysql 5.6 and 5.7 for Debian
#
RUN wget -P /tmp http://dev.mysql.com/get/mysql-apt-config_0.8.0-1_all.deb
#
# Install pre-dependency for debian mysql package
#
RUN apt-get -y install lsb-release
#
# Install mysql 5.7 package
# First, Copy the expect script from the host
#
COPY expect-script-mysql-pkg-config /tmp
RUN /tmp/expect-script-mysql-pkg-config
RUN apt-get -y update
COPY expect-script-mysql-apt-install /tmp
RUN /tmp/expect-script-mysql-apt-install
#
# Replace /etc/nginx/conf.d/default.conf
# and
# Replace /etc/nginx/nginx.conf
#
COPY default.conf /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/nginx.conf
#
# Install PHP Fast CGI Processor
#
RUN apt-get -y install php7.0-fpm
#
# Alter the config of php.ini so that the interpreter will only process the exact file path to avoid security risks
#
RUN sed -i '$ a\# The interpreter will only process the exact file path to avoid security risks' /etc/php/7.0/fpm/php.ini
RUN sed -i '$ a\cgi.fix_pathinfo=0' /etc/php/7.0/fpm/php.ini
#
# Install Composer
#
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === 'aa96f26c2b67226a324c27919f1eb05f21c248b987e6195cad9690d5c1ff713d53020a02ac8c217dbf90a7eacc9d141d') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php --install-dir=/usr/bin --filename=composer
RUN php -r "unlink('composer-setup.php');"
#
# Install Git
#
RUN apt-get -y install git
#
# Copy auth file that is required to download from Magento Repository
#
COPY auth.json /usr/share/nginx/html/auth.json
#
# Set the workkin dir = /usr/share/nginx/html/
# this way we avoid using CD all the time
RUN chown -R www-data:www-data /usr/share/nginx
RUN chown -R www-data:www-data /var/www
WORKDIR /usr/share/nginx/html
#
# Run future commands as user www-data so that all files bellong to him
#
USER www-data
#
# Clone the Magento 2 Git Repository
#
RUN git clone https://github.com/magento/magento2.git
WORKDIR /usr/share/nginx/html/magento2
RUN git checkout tags/2.1.0 -b 2.1.0
#
# Update Magento dependencies
#
RUN composer install
#
ENV MAGENTO_BASE_URL=${MAGENTO_BASE_URL:-http://magento2.local}#
#
# Create the ENTRYPOINT
#
COPY entrypoint.sh /usr/bin/entrypoint.sh
ENTRYPOINT ["/usr/bin/entrypoint.sh"]
#
# Change back to root
#
USER root

