# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Docker machine with a clean Magento 2 instalation
* NGINX:stable (1.10.2) is based on DEBIAN:jessie (8)
* Magento 2.1 + PHP 7.0.13 + MySQL 5.7
* V 0.9
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Ports exposed: HTTP(80) MySQL(3306)
* Usage: export MAGENTO_BASE_URL=http://docker.magento2; docker-compose up magento2 
* will setup magento on the URL set by env var MAGENTO_BASE_URL
* MySQL credentials - username: root; password: root
* Magento admin URL: /admin
* Magento Admin credentials - username: admin password: admin123
* Configuration
* Dependencies: Docker Machine and Docker Compose 1.6.0 or later must be installed

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact